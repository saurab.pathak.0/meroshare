# MeroShare
![](https://meroshare.cdsc.com.np/assets/img/brand-login.png)

This is a library to interact with [meroshare](https://meroshare.cdsc.com.np/) website using python. It holds capability of logging in to the site, staying logged in, tracking all new updates and viewing reports of the shares you have applied, all from a simple script with few self explanatory functions.


### Why library though?
  - The website relies upon session storage to store authorization stuffs, so everytime you restart browser, you are logged out again (*must be for security reasons but bugs me enough to write a library*).
  - I wanted to `pip install` something of my own. My first goto was **helloworld** but turns out somebody else said *hello* to the pip *world* long ago. I needed another idea.
  - This saves 10 minutes of my time each day. 

### Installation
Download using pip via pypi.
```
pip install meroshare
```

### Getting started

  
